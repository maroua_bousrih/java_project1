
public class ForEach
{

	public static void main(String[] args)
	{
      int[] zahlen = new int[] {1,3,5,1,7,55,4,99,44};
      // wert is name der variable
      //zahlen ist feld das ist durchlaufen will
      
      System.out.printf("the sum of the array is: "+ sum(zahlen));
      
//      for (int wert : zahlen) //die wir so oft durchlaufen wir das feld lang ist
//      {
//		System.out.println(wert); //bei jedem durchlauf hat wert den zahlen wert an der Stelle im Feld
//      }
//      
      
	}
	
	public static int sum(int [] feld)
	{   int sum = 0;
		for (int wert:feld)
		{
			sum = sum + wert;
		}
		return(sum);
	}
	
	

}
