
public class ArrayPositionSwap
{
	public static void main(String[] args)
	{
		int[] array= new int[] {41, 5, 9, 10, 7, 3};
		printArray(array);
		System.out.println();
		swap(array, 0,1);
		printArray(array);
		System.out.println();	}
	
	public static void printArray(int[] feld1)
	{
		for(int index=0; index <feld1.length; index++)
		{
			System.out.print(feld1[index]+ " ");
		}
	}
	
	
	// schreiben sie eine methode die in einem Feld zwei Stellen vertauscht
	// die Methode hat das Feld und die erste und zweite stelle als parameter
	
	
	public static void swap(int[] feld, int first, int second)
	{
		int swap=0;
		swap= feld[first];
		feld[first]= feld[second];
		feld[second]=swap;
	}

}
