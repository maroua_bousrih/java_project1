// max aus einem Feld suchen
public class ArrayMax
{

	public static void main(String[] args)
	{
		int[] a = new int[] {2,5,4,7,8,9,7,2};
		int max= findMax(a);
		System.out.println(max);
	}
	public static  int findMax(int[] feld)
	{ 
		int result = feld[0]; //das erste maximum ist der erste Wert aus dem feld
		for(int index=0; index < feld.length; index++)
		{
			if (result < feld[index])
			{
				result=feld[index];
			}
		}
		return result;
	}
}
