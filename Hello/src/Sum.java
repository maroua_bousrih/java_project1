
public class Sum
{
	public static void main(String[] args)
	{
//		int sum = SumUpNumbers(30);
//		System.out.println(sum);
//		System.out.println(SumUpNumbers(600)); //ergebnis einer Methode gleich ausgeben
//		double sumhalb = sumUpHalbe(13);
//		System.out.println(sumhalb);
		System.out.println(sum(10));
		System.out.println(sum2(6));

		
	
	}
	// zB: 1+2+3+4+5+6+7
	public static int SumUpNumbers(int number)
	{
		int result =0;
		for (int index=1; index <= number; index++)
		{
			result = result + index;
		}
		return result;
		
	}
	// zB: 1/2 + 2/2 + 3/2 + 4/2 + 5/2 + 6/2 =========> n/2
	public static double sumUpHalbe(double number)
	{
		double result=0;
		for (double index=1; index <= number; index++) //wenn int benutzen so index/2.0
		{
			result= result + (index/2);
		}
		return result;
	}
	
	// zB: 1/2 + 1/3 + 1/4 + 1/5 ========> 1/n
	
	public static double sum(double number)
	{
		double result=0;
		for (double index=2; index <= number; index++) //wenn int benutzen so index/2.0
		{
			result= result + (1/index);
		}
		return result;
	}
	// zb: 1/2 + 1/4 + 1/6 +1/8 =========> 1/n+2
	public static double sum2(double number)
	{
		double result=0;
		for (double index=2; index <= number; index=index+2) //wenn int benutzen so index/2.0
		{
			result= result + (1/(index));
		}
		return result;
	}
}
